const promiseFactory2 = index =>
    new Promise((resolve, reject) => {
        const timeFail = Math.floor(Math.random() * 10000) + 1000;
        const timeOk = Math.floor(Math.random() * 10000) + 1000;

        setTimeout(() => {
            resolve(`promesa ${index} ok. async-await`);
        }, timeOk);

        /*setTimeout(() => {
            reject(`promesa ${index} fallida. async-await`);
        }, timeFail);*/
    })

/*
// Race las agrupa en una sola
Promise.all(myPromises2)
    .then(res => console.log(res))
    .catch(reason => console.log(reason));*/

async function myAsyncFunc() {
    try {
        let myPromises2 = [];
        for (let i = 0; i < 10; i++) {
            myPromises2 = [...myPromises2, await promiseFactory2(i)];
        }
        console.log('mis promesas 2: ', { myPromises2 });
        return myPromises2;
    } catch (err) {
        console.log('error')
    }
}

function myFunc() {
    return promiseFactory2(2);
}

myAsyncFunc();

myFunc();
