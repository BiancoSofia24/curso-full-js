const getName = () => {return 'Sofia';}

const getLastName = () => {return 'Bianco';}

function getFullName() {
    const name = getName();
    const lastName = getLastName();
    return `${name} ${lastName}`;
}

const fullName = getFullName();
console.log('Nombre completo: ', fullName);