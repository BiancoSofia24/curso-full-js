const myPromise = new Promise((resolve, reject) => {
    const timeFail = Math.floor(Math.random() * 100) + 10;
    const timeOk = Math.floor(Math.random() * 100) + 10;

    console.log('timeOk: ', timeOk);
    console.log('timeFail: ', timeFail);

    setTimeout(() => {
        resolve('promesa ok');
    }, timeOk);

    setTimeout(() => {
        reject('promesa fallida');
    }, timeFail);
})

myPromise.then(
    res => console.log(res),
    reason => console.log(reason)
);

// Hacer múltiples promesas
const promiseFactory = index =>
    new Promise((resolve, reject) => {
        const timeFail = Math.floor(Math.random() * 10000) + 1000;
        const timeOk = Math.floor(Math.random() * 10000) + 1000;

        setTimeout(() => {
            resolve(`promesa ${index} ok. promises`);
        }, timeOk);

        setTimeout(() => {
            reject(`promesa ${index} fallida. promises`);
        }, timeFail);
    })

let myPromises = [];
for (let i = 0; i < 10; i++) {
    myPromises = [...myPromises, promiseFactory(i)];
}

// Para cada una de las promesas hacer...
/*myPromises.forEach(promise =>
    promise
        .then(res => console.log(res))
        .catch(reason => console.log(reason))
);*/

// Agrupa todas las promesas en una sola, en un array, si una falla no muestra el resultado
Promise.all(myPromises)
    .then(res => console.log(res))
    .catch(reason => console.log(reason));