console.log('Hola mundo desde Node.js');

const http = require('http');

// Respuesta del servidor
const serverResponse = (request, response) => {
  response.end('Hola mundo desde server http en Node.js');
}

// Crear servidor
const server = http.createServer(serverResponse);

// Escuchar servidor
server.listen(5000, () => {
  console.log('Server listen requests in port: http://localhost:5000/');
});