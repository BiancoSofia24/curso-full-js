const userTable = document.getElementById('tabla-usuarios');

let user = {};

function getUserID() {
  return location.search.replace('?', '').split('=')[1];
}

function getUser() {
  fetch(`https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios/${getUserID()}`, { method: 'GET' })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      user = data;
      render();
    });
}

function render() {
  const userRender = `<tr>
                        <td class="lead">Nombre:</td>
                        <td> ${(user.nombre) ? user.nombre : 'none'} </td>
                      </tr>
                      <tr>
                        <td class="lead">Apellido:</td>
                        <td> ${(user.apellido) ? user.apellido : 'none'} </td>
                      </tr>`;
  
  userTable.innerHTML = userRender;
}

getUser();