const usersTable = document.getElementById('tabla-usuarios');
const btn = document.getElementById('btn');
const cleanBtn = document.getElementById('btn-limpiar');
const inputName = document.getElementById('nombre');
const inputLname = document.getElementById('apellido');
const index = document.getElementById('indice');

let users = [];
let editButtons = null;
let deleteButtons = null;

// Listar
function render() {
  const usersRender = users
    .map((user, index) => {
      return `<tr>
                <td> ${(user.nombre) ? user.nombre : 'none'} </td>
                <td> ${(user.apellido) ? user.apellido : 'none'} </td>
                <td>
                  <a href="./user.html?usuario=${index}">
                    <button class="btn btn-ver">Ver</button>
                  </a>
                </td>
                <td>
                  <button class="btn btn-editar" data-indice=${index}>Editar</button>
                </td>
                <td>
                  <button class="btn btn-eliminar" data-indice=${index}>Eliminar</button>
                </td>
              </tr>`;
    })
    .join('');
  
  usersTable.innerHTML = usersRender;

  editButtons = document.getElementsByClassName('btn-editar');
  Array.from(editButtons).forEach((editBtn) => {
    editBtn.onclick = editUser;
  });

  deleteButtons = document.getElementsByClassName('btn-eliminar');
  Array.from(deleteButtons).forEach((deleteBtn) => {
    deleteBtn.onclick = deleteUser;
  });
}

// Enviar
function sendInfo(e) {
  e.preventDefault();

  let action = e.target.innerText;
  console.log(action);

  const data = {
    nombre: inputName.value,
    apellido: inputLname.value
  };
  
  let url = null;
  let method = null;
  if (action === 'AGREGAR') {
    url = 'https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios';
    method = 'POST';

  } else if (action === 'EDITAR') {
    if (index.value) {
      url = `https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios/${index.value}`;
      method = 'PUT';

    } else { return; }
  } else { return; }

  fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
    .then((response) => response.json())
    .then((data) => {
      console.log('respjson', data);
      refresh();
      resetForm();
    })
    .catch((error) => {
      console.log(error);
      resetForm();
    });
}

// Eliminar
function deleteUser(e) {
  e.preventDefault();

  fetch(`https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios/${e.target.dataset.indice}`, { 
    method: 'DELETE'
  })
    .then((response) => response.json())
    .then((data) => {
      console.log('respjson', data);
      refresh();
    });
}

// Editar
function editUser(e) {
  e.preventDefault();
  console.log('editUser', e);

  if (e.target.dataset.indice) {
    const user = users[e.target.dataset.indice];
    inputName.value = user.nombre ? user.nombre : '';
    inputLname.value = user.apellido ? user.apellido : '';
    index.value = e.target.dataset.indice;
    btn.innerText = 'editar';
  } else {
    btn.innerText = 'agregar';
  }
}

// Actulizar
function refresh() {
  fetch('https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios', { method: 'GET' })
    .then((response) => response.json())
    .then((data) => {
      users = data;
      render();
    });
}

// Limpiar form
function resetForm() {
  btn.innerText = 'agregar';
  inputName.value = '';
  inputLname.value = '';
}

refresh();

btn.onclick = sendInfo;
cleanBtn.onclick = resetForm;

