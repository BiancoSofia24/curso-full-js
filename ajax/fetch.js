const listUsers2 = document.getElementById('list-users-fetch');
const btn2 = document.getElementById('btn-fetch');
const inputName = document.getElementById('name');
let users = [];

function render() {
    const usersRender = users
		.map((singleUser) => { 
            return `<tr><td> ${singleUser.nombre} </td></tr>`; 
        })
		.join('');
	console.log(usersRender);

	listUsers2.innerHTML = usersRender;
}

function sendInfo() {
    const data = {nombre: inputName.value};

    fetch('https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then((response) => response.json())
    .then((data) => {
        console.log('respjson', data); 
        refresh();
    });
}

function refresh() {
    fetch('https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios', {method: 'GET'})
    .then((response) => response.json())
    .then((data) => {
        users = data;
         render();
    });
}

refresh();

btn2.onclick = sendInfo; 

