const listUsers = document.getElementById('list-users-ajax');
const btn = document.getElementById('btn-ajax');

function reqListener() {
	// Convertimos en objeto JSON
	const users = JSON.parse(this.responseText);
	console.log(users)

	// Renderizar users
	const usersRender = users.map((singleUser) => {
			return `<li> ${singleUser.nombre} </li>`;
		})
		.join('');
	console.log(usersRender);

	listUsers.innerHTML = usersRender;
}

// Petición ajax con un obj xmlhttprequest
var request = new XMLHttpRequest();
request.addEventListener('load', reqListener);
request.open('GET', 'https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios');
request.send();


function sendInfo() {
	request.open('POST', 'https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios', true);
	request.setRequestHeader(
		'Content-type', 
		'application/x-www-form-urlencoded'
	);
	// La petición era con nombre xD Debe ser igual a la api
	request.send('nombre=usuario101');
	setTimeout(refresh, 3000)
}

function refresh() {
	request.open('GET', 'https://bootcamp-dia-3.camilomontoyau.now.sh/usuarios');
	request.send();
}

btn.onclick = sendInfo; 
