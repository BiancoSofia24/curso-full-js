const teamList = document.querySelector('#team-list');
const newBtn = document.querySelector('#new-btn');

const teamFormUI = document.querySelector('#team-form');
const memberName = document.querySelector('#member-name');
const memberLname = document.querySelector('#member-lname');
const positionSelect = document.querySelector('#pos-select');
const saveBtn = document.querySelector('#save-btn');

const memberId = document.querySelector('#index');

let members = [
  {
    name: "Veronica",
    lname: "Requiz",
    pos: "Veterinaria"
  }
];


const ListMembers = () => {
  // Show
  const htmlMembers = members.map((member, index) => {
    return `
            <tr>
               <th scope="row">${index}</th>
               <td>${member.name}</td>
               <td>${member.lname}</td>
               <td>${member.pos}</td>
               <td>
                  <div class="btn-group" role="group">
                     <button type="button" class="btn btn-info btn-sm edit-btn">
                        <i class="fas fa-pencil-alt"></i> Editar
                     </button>
                     <button type="button" class="btn btn-danger btn-sm delete-btn">
                        <i class="fas fa-trash-alt"></i> Eliminar
                     </button>
                  </div>
               </td>
            </tr>
        `;
  }).join('');

  teamList.innerHTML = htmlMembers;

  // Edit btn array
  Array.from(document.getElementsByClassName('edit-btn'))
    .forEach((editMember, index) => {
      editMember.addEventListener('click', EditMember(index));
    });

  // Delete btn array
  Array.from(document.getElementsByClassName('delete-btn'))
    .forEach((deletetMember, index) => {
      deletetMember.addEventListener('click', DeleteMember(index));
    });
}

const sendInfo = (e) => {
  e.preventDefault();

  const info = {
    name: memberName.value,
    lname: memberLname.value,
    pos: positionSelect.value
  }

  const action = saveBtn.innerHTML;
  if (action === 'Editar') {
    members[memberId.value] = info;
  } else {
    members.push(info);
  }

  ListMembers();
}

// Edit
const EditMember = (index) => {
  const editMember = () => {
    // Show modal window for edit
    saveBtn.innerHTML = 'Editar'
    $('#modal-window').modal('toggle');

    const member = members[index];
    memberName.value = member.name;
    memberLname.value = member.lname;
    positionSelect.value = member.pos;
    memberId.value = index;
  }
  return editMember;
}

const ResetModal = () => {
    memberName.value = '';
    memberLname.value = '';
    positionSelect.value = 'Cargo';
    memberId.value = index;
  saveBtn.innerHTML = 'Guardar';
}

const DeleteMember = (index) => {
  const deleteMember = () => {
    members = members.filter((member, memberId) => {
      return (memberId !== index);
    });
    ListMembers();
  }
  return deleteMember;
}

// Show list when the page is loaded
document.addEventListener('DOMContentLoaded', ListMembers);

saveBtn.addEventListener('click', sendInfo);
newBtn.addEventListener('click', ResetModal);

