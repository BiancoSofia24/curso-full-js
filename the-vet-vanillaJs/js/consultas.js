const consultsList = document.querySelector('#consults-list');
const newBtn = document.querySelector('#new-btn');

const consultFormUI = document.querySelector('#consult-form');
const hourSelect = document.querySelector('#hour-select');
const daySelect = document.querySelector('#day-select');
const petName = document.querySelector('#pet-name');
const ownerName = document.querySelector('#owner-name');
const saveBtn = document.querySelector('#save-btn');

const consultId = document.querySelector('#index');

let consults = [
  {
    hour: '10:00',
    day: 'Jueves',
    pet: 'Cafecito',
    owner: 'Niki'
  }
];


const ListConsults = () => {
  // Show
  const htmlConsults = consults.map((consult, index) => {
    return `
            <tr>
               <th scope="row">${index}</th>
               <td>${consult.hour}</td>
               <td>${consult.day}</td>
               <td>${consult.pet}</td>
               <td>${consult.owner}</td>
               <td>
                  <div class="btn-group" role="group">
                     <button type="button" class="btn btn-info btn-sm edit-btn">
                        <i class="fas fa-pencil-alt"></i> Editar
                     </button>
                     <button type="button" class="btn btn-danger btn-sm delete-btn">
                        <i class="fas fa-trash-alt"></i> Eliminar
                     </button>
                  </div>
               </td>
            </tr>
        `;
  }).join('');

  consultsList.innerHTML = htmlConsults;

  // Edit btn array
  Array.from(document.getElementsByClassName('edit-btn'))
    .forEach((editConsult, index) => {
      editConsult.addEventListener('click', EditConsult(index));
    });

  // Delete btn array
  Array.from(document.getElementsByClassName('delete-btn'))
    .forEach((deleteConsult, index) => {
      deleteConsult.addEventListener('click', DeleteConsult(index));
    });
}

const sendInfo = (e) => {
  e.preventDefault();

  const info = {
    hour: hourSelect.value,
    day: daySelect.value,
    pet: petName.value,
    owner: ownerName.value
  }

  const action = saveBtn.innerHTML;
  if (action === 'Editar') {
    consults[consultId.value] = info;
  } else {
    consults.push(info);
  }

  ListConsults();
}

// Edit
const EditConsult = (index) => {
  const editConsult = () => {
    // Show modal window for edit
    saveBtn.innerHTML = 'Editar'
    $('#modal-window').modal('toggle');

    const consult = consults[index];
    hourSelect.value = consult.hour;
    daySelect.value = consult.day;
    petName.value = consult.pet;
    ownerName.value = consult.owner;
    consultId.value = index;
  }
  return editConsult;
}

const ResetModal = () => {
  hourSelect.value = 'Horario';
  daySelect.value = 'Día';
  petName.value = '';
  ownerName.value = '';
  consultId.value = index;
  saveBtn.innerHTML = 'Guardar';
}

const DeleteConsult = (index) => {
  const deleteConsult = () => {
    consults = consults.filter((consult, consultId) => {
      return (consultId !== index);
    });
    ListConsults();
  }
  return deleteConsult;
}

// // Show list when the page is loaded
document.addEventListener('DOMContentLoaded', ListConsults);

saveBtn.addEventListener('click', sendInfo);
newBtn.addEventListener('click', ResetModal);

