const petsList = document.querySelector('#pets-list');
const newBtn = document.querySelector('#new-btn');

const petsFormUI = document.querySelector('#pets-form');
const typeSelect = document.querySelector('#type-select');
const petName = document.querySelector('#pet-name');
const ownerSelect = document.querySelector('#owner-select');
const saveBtn = document.querySelector('#save-btn');

const petId = document.querySelector('#index');

let pets = [
  {
    type: "Otro",
    name: "Cafecito",
    owner: "Niki"
  },
  {
    type: "Roedor",
    name: "Musli",
    owner: "Alejandra"
  }
];

const ListPets = () => {
  // Show
  const htmlPets = pets.map((pet, index) => {
    return `
            <tr>
               <th scope="row">${index}</th>
               <td>${pet.type}</td>
               <td>${pet.name}</td>
               <td>${pet.owner}</td>
               <td>
                  <div class="btn-group" role="group">
                     <button type="button" class="btn btn-info btn-sm edit-btn">
                        <i class="fas fa-pencil-alt"></i> Editar
                     </button>
                     <button type="button" class="btn btn-danger btn-sm delete-btn">
                        <i class="fas fa-trash-alt"></i> Eliminar
                     </button>
                  </div>
               </td>
            </tr>
        `;
  }).join('');

  petsList.innerHTML = htmlPets;

  // Edit btn array
  Array.from(document.getElementsByClassName('edit-btn'))
    .forEach((editBtn, index) => {
      editBtn.addEventListener('click', EditPet(index));
    });
  
  // Delete btn array
  Array.from(document.getElementsByClassName('delete-btn'))
    .forEach((deletetBtn, index) => {
      deletetBtn.addEventListener('click', DeletePet(index));
    });

}

const sendInfo = (e) => {
  e.preventDefault();

  const info = {
    type: typeSelect.value,
    name: petName.value,
    owner: ownerSelect.value
  }

  const action = saveBtn.innerHTML;
  if (action === 'Editar') {
    pets[petId.value] = info;
  } else {
    pets.push(info);
  }

  ListPets();
}

// Edit
const EditPet = (index) => {
  const editPet = () => {
    // Show modal window for edit
    saveBtn.innerHTML = 'Editar'
    $('#modal-window').modal('toggle');

    const pet = pets[index];
    typeSelect.value = pet.type;
    petName.value = pet.name;
    ownerSelect.value = pet.owner;
    petId.value = index;
  }
  return editPet;
}

const ResetModal = () => {
  typeSelect.value = 'Tipo de mascota';
  petName.value = '';
  ownerSelect.value = 'Dueño';
  petId.value = '';
  saveBtn.innerHTML = 'Guardar';
}

const DeletePet = (index) => {
  const deletePet = () => {
    pets = pets.filter((pet, petId) => {
      return (petId !== index);
    });
    ListPets();
  }
  return deletePet;
}

// Show list when the page is loaded
document.addEventListener('DOMContentLoaded', ListPets);

saveBtn.addEventListener('click', sendInfo);
newBtn.addEventListener('click', ResetModal);
