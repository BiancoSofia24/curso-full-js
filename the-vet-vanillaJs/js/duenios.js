const ownersList = document.querySelector('#owners-list');
const newBtn = document.querySelector('#new-btn');

const ownersFormUI = document.querySelector('#owners-form');
const ownerName = document.querySelector('#owner-name');
const ownerLname = document.querySelector('#owner-lname');
const petName = document.querySelector('#pet-name');
const typeSelect = document.querySelector('#type-select');
const saveBtn = document.querySelector('#save-btn');

const ownerId = document.querySelector('#index');

let owners = [
  {
    name: "Niki",
    lname: "Duran",
    pet: "Cafecito",
    type: "Otro"
  },
  {
    name: "Alejandra",
    lname: "Hermida",
    pet: "Musli",
    type: "Roedor"
  }
];


const ListOwners = () => {
  // Show
  const htmlOwners = owners.map((owner, index) => {
    return `
            <tr>
               <th scope="row">${index}</th>
               <td>${owner.name}</td>
               <td>${owner.lname}</td>
               <td>${owner.pet}</td>
               <td>${owner.type}</td>
               <td>
                  <div class="btn-group" role="group">
                     <button type="button" class="btn btn-info btn-sm edit-btn">
                        <i class="fas fa-pencil-alt"></i> Editar
                     </button>
                     <button type="button" class="btn btn-danger btn-sm delete-btn">
                        <i class="fas fa-trash-alt"></i> Eliminar
                     </button>
                  </div>
               </td>
            </tr>
        `;
  }).join('');

  ownersList.innerHTML = htmlOwners;

  // Edit btn array
  Array.from(document.getElementsByClassName('edit-btn'))
    .forEach((editOwner, index) => {
      editOwner.addEventListener('click', EditOwner(index));
    });

  // Delete btn array
  Array.from(document.getElementsByClassName('delete-btn'))
    .forEach((deletetOwner, index) => {
      deletetOwner.addEventListener('click', DeleteOwner(index));
    });
}

const sendInfo = (e) => {
  e.preventDefault();

  const info = {
    name: ownerName.value,
    lname: ownerLname.value,
    pet: petName.value,
    type: typeSelect.value
  }

  const action = saveBtn.innerHTML;
  if (action === 'Editar') {
    owners[ownerId.value] = info;
  } else {
    owners.push(info);
  }

  ListOwners();
}

// Edit
const EditOwner = (index) => {
  const editOwner = () => {
    // Show modal window for edit
    saveBtn.innerHTML = 'Editar'
    $('#modal-window').modal('toggle');

    const owner = owners[index];
    ownerName.value = owner.name;
    ownerLname.value = owner.lname;
    petName.value = owner.pet;
    typeSelect.value = owner.type;
    ownerId.value = index;
  }
  return editOwner;
}

const ResetModal = () => {
  ownerName.value = '';
  ownerLname.value = '';
  petName.value = '';
  typeSelect.value = 'Tipo de mascota';
  ownerId.value = '';
  saveBtn.innerHTML = 'Guardar';
}

const DeleteOwner = (index) => {
  const deleteOwner = () => {
    owners = owners.filter((owner, ownerId) => {
      return (ownerId !== index);
    });
    ListOwners();
  }
  return deleteOwner;
}

// Show list when the page is loaded
document.addEventListener('DOMContentLoaded', ListOwners);

saveBtn.addEventListener('click', sendInfo);
newBtn.addEventListener('click', ResetModal);

